import * as jQuery from 'jquery';
import { SignerService } from './SignerService';

declare global {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    interface JQuery {
        signer(service: SignerService | 'getService'): JQuery<HTMLElement>;
        signer(method: 'getService'): SignerService;
    }
}
// noinspection JSUnusedGlobalSymbols
jQuery.fn.extend({
    signer: function signer(service: SignerService | 'getService') {
        // eslint-disable-next-line @typescript-eslint/no-this-alias,@typescript-eslint/no-unsafe-assignment
        const element: JQuery<HTMLElement> = this;
        if (service === 'getService') {
            return element.data('SignerService') as SignerService;
        }
        element.data('SignerService', service);
        return element;
    }
});