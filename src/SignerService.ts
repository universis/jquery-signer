import * as jQuery from 'jquery';

export interface SignatureInspectorConfiguration {
    text?: string[];
    textPosition?: string;
    width?: number;
    height?: number;
}

export interface SignerServiceConfiguration {
    timestampServer?: string;
    defaultLocation?: string;
    inspector?: SignatureInspectorConfiguration;
    use?: 'SignerService' | 'HaricaSignerService' | string;
    configurable?: boolean;
    verificationUrl?: string;
}

export interface Certificate {
    version: number;
    subjectDN: string;
    sigAlgName?: string;
    sigAlgOID?: string;
    issuerDN: string;
    serialNumber: any;
    notAfter: string;
    notBefore: string;
    expired: boolean;
    thumbprint: string;
    commonName: string;
}

export interface VerifySignatureResult {
    valid: boolean;
    certificates: Certificate[];
    signatureProperties?: {
        signingDate: string;
        reason: string;
        signingCertificate: Certificate
    };
}


export abstract class SignerService {

    static SINGER_URI = 'http://localhost:2465/';
    static DEFAULT_SIGN_POSITION = '20,10,320,120'


    protected signerAuthorization: string;
    constructor() {
        //
    }

    public abstract get status(): Promise<any>;

    public abstract authenticate({ username, password, rememberMe }: { username?: string; password?: string; rememberMe?: boolean; }): Promise<boolean>;

    public get authorization() {
        return sessionStorage.getItem('signer.auth') || this.signerAuthorization;
    }

    public abstract getCertificates(force?: boolean): Promise<Certificate[]>;

    public abstract queryStatus(): Promise<any>;

    public abstract sign(form: FormData): Promise<any>;

    public abstract requestVerificationCode(): Promise<boolean>;

    setLastCertificate(thumbprint: string) {
        return sessionStorage.setItem('signer.lastCertificate', thumbprint);
    }

    getLastCertificate(): string {
        return sessionStorage.getItem('signer.lastCertificate');
    }

    destroy() {
        if (this.signerAuthorization) {
            this.signerAuthorization = null;
        }
    }

    getInspectorUrl(): string {
        return new URL('/signature/inspect', SignerService.SINGER_URI).toString();
    }

    protected async inspectPosition(blob: Blob, configuration: SignatureInspectorConfiguration): Promise<any> {
        const formData = new FormData();
        // set name
        if (configuration.text != null) {
            configuration.text.forEach((value) => {
                formData.append('text', encodeURIComponent(value));
            });
        }
        if (configuration.textPosition != null) {
            formData.append('textPosition', configuration.textPosition);
        }
        if (configuration.width != null) {
            formData.append('width', configuration.width.toFixed(0));
        }
        if (configuration.height != null) {
            formData.append('height', configuration.height.toFixed(0));
        }
        // set file
        formData.append('file', blob, 'inspect-position.pdf');
        const headers = {};
        return new Promise((resolve, reject) => {
            void jQuery.ajax({
                url: this.getInspectorUrl(),
                headers,
                method: 'POST',
                crossDomain: true,
                data: formData,
                dataType: 'text',
                xhrFields: {
                    withCredentials: true
                }
            }).done((data: any) => {
                return resolve(data);
            }).fail((jqXHR, textStatus, errorThrown) => {
                const error = new Error(errorThrown);
                Object.assign({
                    status: jqXHR.status
                });
                return reject(error);
            });
        });

    }

    public abstract get requiresVerificationCode(): boolean;

    public abstract get requiresVerificationCodeRequest(): boolean;

    public abstract confirmVerificationCode(): Promise<any>;

    public abstract get requiresUsernamePassword(): boolean;

    public abstract getVerificationUrl(): string;

    public abstract verifyDocument(file: any): Promise<VerifySignatureResult[]>;
}
