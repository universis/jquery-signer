import * as jQuery from 'jquery';
import { Certificate, SignerService, VerifySignatureResult } from './SignerService';
import { UsernameAndPassword } from './UsernameAndPassword';

export class DocuSignSignerService extends SignerService {

    public requestVerificationCode(): Promise<boolean> {
        return new Promise((resolve, reject) => {
            void jQuery.ajax({
                url: new URL('requestCode', this.config.serviceRoot).toString(),
                method: 'POST',
                headers: {
                    'Authorization': this.authorization
                },
                crossDomain: true,
                dataType: 'json',
                xhrFields: {
                    withCredentials: false
                }
            }).done((result) => {
                if (result === 1) {
                    return resolve(false);
                }
                if (result === 9) {
                    return reject(new Error('An error occurred while sending verification code'));
                }
                return resolve(true);
            }).fail((jqXHR, textStatus, errorThrown) => {
                const error = new Error(errorThrown);
                Object.assign({
                    status: jqXHR.status
                });
                return reject(error);
            });
        });
    }
    public get requiresVerificationCodeRequest(): boolean {
        return true;
    }

    private certificates: Certificate[] | null = null;

    constructor(public config: {serviceRoot: string}) {
        super();
    }

    public get status(): Promise<any> {
        throw new Error('Method not implemented.');
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    public authenticate({ username, password, rememberMe }: { username?: string; password?: string; rememberMe?: boolean; }): Promise<boolean> {
        this.certificates = [];
        return new Promise((resolve, reject) => {
            const tryAuthorize = UsernameAndPassword.create(username, password)
            void jQuery.ajax({
                url: new URL('keystore/certs', this.config.serviceRoot).toString(),
                headers: {
                    'Authorization': tryAuthorize
                },
                method: 'GET',
                crossDomain: true,
                dataType: 'json',
                xhrFields: {
                    withCredentials: false
                }
            }).done((certificates: Certificate[]) => {
                this.signerAuthorization = tryAuthorize;
                this.certificates = certificates;
                return resolve(true);
            }).fail((jqXHR, textStatus, errorThrown) => {
                const error = new Error(errorThrown);
                Object.assign({
                    status: jqXHR.status
                });
                return reject(error);
            });
        });
    }
    public getCertificates(force?: boolean): Promise<Certificate[]> {
        if (force) {
            this.certificates = null;
        }
        if (Array.isArray(this.certificates)) {
            return Promise.resolve(this.certificates);
        }
        return new Promise((resolve, reject) => {
            void jQuery.ajax({
                url: new URL('keystore/certs', this.config.serviceRoot).toString(),
                headers: {
                    'Authorization': this.authorization
                },
                method: 'GET',
                crossDomain: true,
                dataType: 'json',
                xhrFields: {
                    withCredentials: false
                }
            }).done((cert: Certificate) => {
                this.certificates = []
                if (cert) {
                    this.certificates.push(cert);
                }
                return resolve(this.certificates);
            }).fail((jqXHR, textStatus, errorThrown) => {
                const error = new Error(errorThrown);
                Object.assign({
                    status: jqXHR.status
                });
                return reject(error);
            });
        });
    }
    public queryStatus(): Promise<any> {
        return new Promise((resolve, reject) => {
            void jQuery.ajax({
                url: new URL('keystore/certs', this.config.serviceRoot).toString(),
                method: 'OPTIONS',
                crossDomain: true,
                xhrFields: {
                    withCredentials: false
                }
            }).done(() => {
                return resolve(true);
            }).fail((jqXHR, textStatus, errorThrown) => {
                const error = new Error(errorThrown);
                Object.assign({
                    status: jqXHR.status
                });
                return reject(error);
            });
        });
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    public async sign(form: FormData): Promise<Blob> {
        // create message
        const result: Blob = await new Promise((resolve, reject) => {
            void jQuery.ajax({
                url: new URL('sign', this.config.serviceRoot).toString(),
                method: 'POST',
                cache: false,
                contentType: false,
                processData: false,
                headers: {
                    'Authorization': this.authorization
                },
                crossDomain: true,
                data: form,
                xhrFields: {
                    withCredentials: false,
                    responseType: 'blob'
                }
            }).done((data: Blob) => {
                return resolve(data);
            }).fail((jqXHR, textStatus, errorThrown) => {
                const error = new Error(errorThrown);
                Object.assign({
                    status: jqXHR.status
                });
                return reject(error);
            });
        });
        return result;
    }

    public get requiresVerificationCode(): boolean {
        return true;
    }
    public confirmVerificationCode(): Promise<any> {
        throw new Error('Method not implemented.');
    }
    public get requiresUsernamePassword(): boolean {
        return true;
    }
    public getVerificationUrl(): string {
        throw new Error('Method not implemented.');
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    public verifyDocument(file: any): Promise<VerifySignatureResult[]> {
        throw new Error('Method not implemented.');
    }

}