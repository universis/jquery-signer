export class UsernameAndPassword {
    constructor(private value: string) {
        //
    }
    get username(): string {
        const decrypted = atob(this.value.replace(/^Basic\s/g, ''));
        const separator = decrypted.indexOf(':');
        return decrypted.substr(0, separator);
    }

    get password(): string {
        const decrypted = atob(this.value.replace(/^Basic\s/g, ''));
        const separator = decrypted.indexOf(':');
        return decrypted.substr(separator + 1);
    }

    static create(username: string, password: string) {
        return 'Basic ' + btoa(`${username}:${password}`);
    }

}
