
# @universis/jquery-signer


# Usage

    npm i @universis/jquery-signer

## Load plugin

```html
<body>
    <form id="form1" novalidate>
        <div class="form-group mt-2">
            <label for="username">Email address</label>
            <input
            type="email"
            class="form-control"
            id="username"
            aria-describedby="emailHelp"
            placeholder="Enter username"
            />
            <small id="emailHelp" class="form-text text-muted"
            >We'll never share your email with anyone else.</small
            >
        </div>
        <div class="form-group mt-2">
            <label for="password">Password</label>
            <input
            type="password"
            class="form-control"
            id="password"
            placeholder="Password"
            />
        </div>
        <button type="submit" class="mt-2 btn btn-primary">
            Get certificates
        </button>
    </form>
</body>
...
  <script src="https://www.unpkg.com/jquery@3.7.1/dist/jquery.js"></script>
  <script src="https://www.unpkg.com/@universis/jquery-signer@1.0.0/dist/index.umd.js"></script>
```

## Use signer service

```javascript
jQuery(document).ready(function () {
  var require = function require(id) {
    return window[id];
  };
  var DocuSignSignerService =
    require('@universis/jquery-signer').DocuSignSignerService;
  jQuery(document).signer(
    new DocuSignSignerService({
      serviceRoot: 'http://localhost:2400',
    })
  );
  var $form = jQuery('#form1');
  $form.on('submit', function (event) {
    event.preventDefault();
    var signer = jQuery(document).signer('getService');
    var username = $form.find('#username').val();
    var password = $form.find('#password').val();
    signer
      .authenticate({
        username: username,
        password: password,
      })
      .then(function (result) {
        signer.getCertificates().then(function (certs) {
          console.log(certs);
        });
      })
      .catch(function (err) {
        console.error(err);
      });
  });
});
```
