import * as jQuery from 'jquery';
import '../src/index';
import { Certificate, DocuSignSignerService } from '../src/index';

const username = 'user';
const password = 'password';
const otp = '123456';
const SERVICE_ROOT = 'http://localhost:2465/';

describe('SignerService', () => {
    it('should create instance', () => {
        //
        jQuery(document).signer(new DocuSignSignerService({
            serviceRoot: SERVICE_ROOT
        }));
        const service  = jQuery(document).signer('getService');
        expect(service).toBeTruthy();
    });

    it('should get status', async () => {
        //
        jQuery(document).signer(new DocuSignSignerService({
            serviceRoot: SERVICE_ROOT
        }));
        const service  = jQuery(document).signer('getService');
        // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
        const status = await service.queryStatus();
        expect(status).toBeTruthy();
    });

    it('should authenticate', async () => {
        //
        jQuery(document).signer(new DocuSignSignerService({
            serviceRoot: SERVICE_ROOT
        }));
        const service  = jQuery(document).signer('getService');
        // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
        const result = await service.authenticate({
            username,
            password
        })
        expect(result).toBeTruthy();
    });

    it('should get file as blob', async () => {
        const blob = await new Promise((resolve, reject) => {
            void jQuery.ajax({
                url: '/base/spec/assets/lorem-ipsum.pdf',
                cache:false,
                xhrFields:{
                    responseType: 'blob'
                },
                success: (data) => {
                    resolve(data)
                },
                error: (jqXHR, textStatus, errorThrown) => {
                    reject(new Error(errorThrown))
                }
            });
        })
        expect(blob).toBeTruthy();
    });

    fit('should get certificates', async () => {
        //
        jQuery(document).signer(new DocuSignSignerService({
            serviceRoot: SERVICE_ROOT
        }));
        const service  = jQuery(document).signer('getService');
        // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
        await service.authenticate({
            username,
            password
        })
        const results: Certificate[] = await service.getCertificates();
        expect(results).toBeTruthy();
        expect(results).toBeInstanceOf(Array);
        expect(results.length).toBeGreaterThan(0);
    });

    it('should request verification code', async () => {
        //
        jQuery(document).signer(new DocuSignSignerService({
            serviceRoot: SERVICE_ROOT
        }));
        const service  = jQuery(document).signer('getService');
        // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
        await service.authenticate({
            username,
            password
        })
        const result = await service.requestVerificationCode();
        expect(result).toBeTruthy();
    });

    fit('should sign document', async () => {
        //
        jQuery(document).signer(new DocuSignSignerService({
            serviceRoot: SERVICE_ROOT
        }));
        const service  = jQuery(document).signer('getService');
        // authenticate
        await service.authenticate({
            username,
            password
        })
        // create form
        const form = new FormData();
        // await service.requestVerificationCode();

        const blob: Blob = await new Promise((resolve, reject) => {
            void jQuery.ajax({
                url: '/base/spec/assets/lorem-ipsum.pdf',
                cache:false,
                xhrFields:{
                    responseType: 'blob'
                },
                success: (data: Blob) => {
                    resolve(data)
                },
                error: (jqXHR, textStatus, errorThrown) => {
                    reject(new Error(errorThrown))
                }
            });
        })
        // set otp
        form.set('otp', otp as unknown as string);
        form.set('timestampServer', 'https://timestamp.aped.gov.gr/qtss');
        // set file
        form.set('file', blob, 'lorem-ipsum.pdf')
        const result: unknown = await service.sign(form);
        expect(result).toBeTruthy();
    });
});
